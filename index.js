const startTour = () => {
    // 1) list all targets 
    // 2) create component with backdrop and content
    // 3) calculate position of target
    // 4) position component to target element
    document.body.appendChild(guide);
    guide.appendChild(guest);
    guide.style.display = 'block';
    guide.setAttribute('class', 'backdrop');
    if (!document.getElementById('card-tpl')) {
        const tpl = document.querySelector("#card");
        const clone = tpl.content.cloneNode(true);
        guide.appendChild(clone);
    }
    console.log(guide);
    bindEvents();
    service.initate();
};
const guide = document.createElement('div');
const guest = document.createElement('div');
guest.classList.add('guest');
function bindEvents() {
    const closeButton = document.getElementById('close-btn');
    closeButton.addEventListener('click', function () {
        guide.style.display = 'none';
        service.resetStepper();
    });
    const nextButton = document.getElementById('next-btn');
    nextButton.addEventListener('click', function () {
        service.stepNext();
    });
    const previousButton = document.getElementById('prev-btn');
    previousButton.addEventListener('click', function () {
        service.stepBack();
    });
}
class Position {
    constructor(h, w, t, l) {
        this.height = h;
        this.width = w;
        this.top = t;
        this.left = l;
    }
}
class DomService {
    assignPositionsToGuest(el) {
        let hostPosition = new Position(el.clientHeight, el.clientWidth, el.offsetTop, el.offsetLeft);
        guest.style.width = hostPosition.width;
        guest.style.height = hostPosition.height;
        guest.style.top = hostPosition.top;
        guest.style.left = hostPosition.left;
        this.bindCard(hostPosition);
    }
    bindCard(position) {
        const el = document.getElementById('card-tpl');
        el.style.top = (parseInt(position.top) + 12 + parseInt(position.height)).toString();
        el.style.left = position.left;
        el.scrollIntoView();
    }
    getSortedElementList() {
        let sorted = [];
        let list = document.querySelectorAll('[data-tourer="true"]');
        list.forEach((el) => {
            const value = el.attributes.getNamedItem('data-step').value;
            sorted.push({ value, el });
        });
        list = Array(sorted.length).fill(0);
        sorted.forEach(el => {
            list[el.value] = el.el;
        });
        list.shift();
        return list;
    }
}
class DomPositionCalculator {
    constructor(domService) {
        this.domService = domService;
        this.elementList = [];
        this.step = -1;
    }
    findElementandPlace() {
        const el = this.elementList[this.step];
        this.domService.assignPositionsToGuest(el);
    }
    initate() {
        this.elementList = this.domService.getSortedElementList();
        this.stepNext();
    }
    stepNext() {
        if ((this.elementList.length - 1) === this.step) {
            return;
        }
        this.step += 1;
        this.findElementandPlace();
    }
    resetStepper() {
        this.step = -1;
    }
    stepBack() {
        if ((this.step - 1) < 0) {
            return;
        }
        this.step = this.step - 1;
        this.findElementandPlace();
    }
}
let service = new DomPositionCalculator(new DomService());
